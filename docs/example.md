## Configuration example:

```
mikrotik_scheduled_config_rollback:
  password_path: "credentials/" # Path to save files with passwords for backup files. Password files have timestamp suffix
  reload_in: 30m # Time to schedule reload in
  backup_download_path: "config_backups/" # Path to save downloaded backup files. The files have the same timestamp suffix as the password files related to them
```

## Create a task in the beginning of your site.yml to schedule reload on fail
```
- name: Schedule Reload
  gather_facts: yes # Required because I use ansible_date_time.iso8601_basic to create file names
  hosts: routeros
  tasks:
    - import_role:
        name: mikrotik-scheduled-configuration-rollback
  vars:
    mikrotik_rollback_backup_action: schedule # Flag to show that we schedule restore and make backup
```

## Create a task in the end of your site.yml to cancel reload

```
- name: Cancel Scheduled Reload
  gather_facts: yes # Required because I use ansible_date_time.iso8601_basic to create file names
  hosts: routeros
  tasks:
    - import_role:
        name: mikrotik-scheduled-configuration-rollback
  vars:
    mikrotik_rollback_backup_action: cancel # Flag to show that we are cancelling scheduled restore
```
